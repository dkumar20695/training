import java.util.HashMap;

public class LongestCommonSubsequence {

    private HashMap<Character, Integer> characterCountMap = new HashMap<Character, Integer>();

    public static void main(String[] args) {

        LongestCommonSubsequence obj = new LongestCommonSubsequence();

        String str1 = "abbcdgf";
        String str2 = "bbadcgf";

        System.out.println(obj.calculateSubSequence(str1, str2, str1.length(), str2.length()));
    }

    public int calculateSubSequence(String str1, String str2, int len1, int len2) {

        //We have reached the end of one or both of the strings
        if(len1 == 0 || len2 == 0) {

            return 0;
        }

        //If the characters at the same position match, then reduce both lengths by 1
        if(str1.charAt(len1 - 1) == str2.charAt(len2 - 1)) {

            return 1 + calculateSubSequence(str1, str2, len1 - 1, len2 - 1);
        }

        //If the characters dont match, create 2 branches to the same "tree".
        // One with the left side string length - 1 and the other with the right side string length - 1
        return Math.max(calculateSubSequence(str1, str2, len1 - 1, len2),
                calculateSubSequence(str1, str2, len1, len2 - 1));
    }
}
